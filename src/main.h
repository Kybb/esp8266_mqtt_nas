//#########################################################
//###                     INCLUDE                       ###
//#########################################################
#pragma once
#include <Arduino.h>


//#########################################################
//###                     DEFINE                        ###
//#########################################################
#define SERIAL_DEBUG
//#define TEST_PUB
#define WIFI_WORK
//#define WIFI_HOME
#define DHT_ON
//#define BME_ON
#ifdef DHT_ON
  //#define DHT_PIN   0     // D3 на плате LoLIN
  #define DHT_PIN   2       // ESP-01 и шилд DHT11
#endif

//#########################################################
//###                   VARIABLES                       ###
//#########################################################
// для выполнения опроса датчика и отправки по mqtt раз в надцать секунд
long lastMsg = 0;
long now = 0;

#ifdef DHT_ON
  float humidity =0, temperature =0;        // переменные для DHT
#endif

#ifdef BME_ON
  float bme_temp, bme_hum, bme_pres;        // данные от BME280
#endif

char char_Number[10];                       // Переменная для перевода температуры, влажности и всего что потребуется из float в char

// переменные для тестового вывода в тестовый топик
#ifdef TEST_PUB
    uint16_t value;                 // счетчик сообщений для вывода в рамках TEST_PUB
    char msg[200];
#endif