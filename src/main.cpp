//#########################################################
//###                     INCLUDE                       ###
//#########################################################
#include <Arduino.h>
#include "main.h"
#include "pass.h"           // адреса, пароли и пр. персональные данные, что не нужно отдавать в git
#include <PubSubClient.h>   // комплектная из репозитория PlatformIO
#include <ESP8266WiFi.h>    // комплектная из репозитория PlatformIO
#ifdef DHT_ON
  #include <DHT.h>          // комплектная из репозитория PlatformIO или https://github.com/markruys/arduino-DHT
#endif
#ifdef BME_ON
  #include <BME280I2C.h>
  #include <Wire.h>
  #include <SPI.h>
#endif


//#########################################################
//###                  initialization                   ###
//#########################################################
WiFiClient espClient;             // для инициализации WiFi
PubSubClient client(espClient);   // для инициализации mqtt

#ifdef DHT_ON                     // для инициализации DHT
  DHT dht;
#endif

#ifdef BME_ON                     // для инициализации BME
   BME280I2C::Settings settings(
   BME280::OSR_X1,
   BME280::OSR_X1,
   BME280::OSR_X1,
   BME280::Mode_Forced,
   BME280::StandbyTime_1000ms,
   BME280::Filter_Off,
   BME280::SpiEnable_False,
   0x76 // I2C address. I2C specific.
  );

  BME280I2C bme(settings);

  BME280::TempUnit tempUnit(BME280::TempUnit_Celsius);
  //BME280::PresUnit presUnit(BME280::PresUnit_Pa);
  BME280::PresUnit presUnit(BME280::PresUnit_inHg);
#endif


//#########################################################
//###                    Функции                        ###
//#########################################################
// установка WIFI соединения
void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  #ifdef SERIAL_DEBUG
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(ssid);
  #endif

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    #ifdef SERIAL_DEBUG
      Serial.print(".");
    #endif
  }

  #ifdef SERIAL_DEBUG
    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
  #endif
}

/*
void callback(char* topic, byte* payload, unsigned int length) {
  #ifdef SERIAL_DEBUG
    Serial.print("Message arrived [");
    Serial.print(topic);
    Serial.print("] ");

    for (int i = 0; i < length; i++) {
      Serial.print((char)payload[i]);
    }
    Serial.println();
  #endif
}
*/


// функция переподключения к WiFi и к mqtt
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    #ifdef SERIAL_DEBUG
      Serial.print("Attempting MQTT connection...");
    #endif
    // Create a random client ID
    // Этот кусок я пролюбил в основном проекте, надо или самому назначать уникальные имена клиентов или оставить этот кусок кода и он будет генерить рандом
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    //if (client.connect("ESP8266Client")) {
    // @ проверить работу авторизации mqtt
    if ( client.connect( clientId.c_str(), mqtt_user, mqtt_pass ) ) {
      #ifdef SERIAL_DEBUG
        Serial.println("connected");
      #endif
      // Once connected, publish an announcement...
      #ifdef TEST_PUB
        client.publish("test/outTopic", "hello world");
      #endif
      // ... and resubscribe
      client.subscribe("test/inTopic");
    } else {
      #ifdef SERIAL_DEBUG
        Serial.print("failed, rc=");
        Serial.print(client.state());
        Serial.println(" try again in 5 seconds");
      #endif
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}


//#########################################################
//###                       SETUP                       ###
//#########################################################
void setup() {
    #ifdef SERIAL_DEBUG
      Serial.begin(9600);
      while(!Serial) {}     // ждем инициализации
      Serial.println("Serial DEBUG");
    #endif

    #ifdef DHT_ON
      dht.setup(DHT_PIN);                     // Иниицализация DHT, пин для DHT через запятую можно указать тип датчика, например DHT22
    #endif

    #ifdef BME_ON
      Wire.begin();                             // Иниицализация I2C
      bme.begin();                              // Иниицализация BME280
      // Change some settings before using.
      settings.tempOSR = BME280::OSR_X4;
      bme.setSettings(settings);
      #ifdef SERIAL_DEBUG
        switch(bme.chipModel())  {
            case BME280::ChipModel_BME280:
              Serial.println("Found BME280 sensor! Success.");
            break;
            case BME280::ChipModel_BMP280:
              Serial.println("Found BMP280 sensor! No Humidity available.");
            break;
            default:
              Serial.println("Found UNKNOWN sensor! Error!");
        }
      #endif
    #endif

    setup_wifi();
    client.setServer(mqtt_server, MQTT_PORT); // указание IP/домена сервера MQTT и порта
    // @ отключил подписку для теста
    //client.setCallback(callback);             // кажется это подписка на входящие сообщения
}


//#########################################################
//###                       LOOP                        ###
//#########################################################
void loop() {
    if (!client.connected()) {
      reconnect();
    }
    client.loop();

    now = millis();
    if (now - lastMsg > 20000) {      // аналог задержки в 20с, но более умный способ, т.к. loop продолжает крутится и если мы отслеживаем наличие изменений в mqtt или еще где, то это отличный способ
      lastMsg = now;

      #ifdef DHT_ON
        delay(dht.getMinimumSamplingPeriod());  // что-то для библиотеки DHT
        temperature = dht.getTemperature();
        humidity = dht.getHumidity();
      #endif

      #ifdef BME_ON
        //delay(1000);
        bme.read(bme_pres, bme_temp, bme_hum, tempUnit, presUnit);  // читаем из датчика BME
      #endif

      #if defined(SERIAL_DEBUG) && defined(DHT_ON)
        Serial.print("DHT status is - ");
        Serial.println(dht.getStatusString());  // статус опроса DHT
        Serial.print(humidity);
        Serial.print("%\t");
        
        Serial.print(temperature, 1);
        Serial.print("C");
        Serial.print("\t");

        Serial.println();
        Serial.println();
        Serial.flush();  // ждем пока все выплюнем из буфера и только потом чет делать
      #endif

      #if defined(SERIAL_DEBUG) && defined(BME_ON)
        Serial.println("BME");
        Serial.print(bme_hum, 1);
        Serial.print("%\t");
        
        Serial.print(bme_temp, 1);
        Serial.print("C");
        Serial.print("\t");

        Serial.print(bme_pres*25.4, 1);
        Serial.print("mmHg");
        Serial.print("\t");

        Serial.println();
        Serial.println();
        Serial.flush();  // ждем пока все выплюнем из буфера и только потом чет делать
      #endif

      #ifdef DHT_ON
        /*
          dtostrf(floatvar, StringLengthIncDecimalPoint, numVarsAfterDecimal, charbuf);
            где:
                floatvar - преобразуемая переменная типа float;
                StringLengthIncDecimalPoint - длина получаемого символьного значения;
                numVarsAfterDecimal - количество символов после запятой;
                charbuf - символьный массив для сохранения результата преобразования
        */
        dtostrf(temperature, 3, 1, char_Number);       // Перевод в char
        client.publish("sensor1/temp", char_Number);   //  отправка по MQTT сообщения

        dtostrf(humidity, 3, 1, char_Number);          // Перевод в char
        client.publish("sensor1/hum", char_Number);    //  отправка по MQTT сообщения   
      #endif  

      #ifdef BME_ON
        dtostrf(bme_temp, 3, 1, char_Number);          // Перевод в char
        client.publish("sensor2/temp", char_Number);   //  отправка по MQTT сообщения

        dtostrf(bme_hum, 3, 1, char_Number);           // Перевод в char
        client.publish("sensor2/hum", char_Number);    //  отправка по MQTT сообщения

        dtostrf(bme_pres*25.4, 3, 1, char_Number);          // Перевод в char
        client.publish("sensor2/pres", char_Number);    //  отправка по MQTT сообщения
      #endif

      #ifdef TEST_PUB
        ++value;
        snprintf (msg, 75, "hello world #%ld", value);
        #ifdef SERIAL_DEBUG
          Serial.print("Publish message: ");
          Serial.println(msg);
        #endif  
        client.publish("test/outTopic", msg);
      #endif
    }
}