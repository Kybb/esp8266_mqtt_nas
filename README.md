# esp8266_mqtt_nas

В pass.h записи вида:
#ifdef WIFI_WORK
  const char* ssid = "aaaaa";
  const char* password = "1111111";
  const char* mqtt_server = "some.domain.com";
  const char* mqtt_user = "test_user"; // Логи от сервер
  const char* mqtt_pass = "test_pass"; // Пароль от сервера
  #define MQTT_PORT  3881
#endif

#ifdef WIFI_HOME
  const char* ssid = "aaaaa";
  const char* password = "1111111";
  const char* mqtt_server = "192.168.0.123";
  const char* mqtt_user = "test_user"; // Логи от сервер
  const char* mqtt_pass = "test_pass"; // Пароль от сервера
  #define MQTT_PORT  1883
#endif